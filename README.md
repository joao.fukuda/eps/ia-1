# Exercício Programa de Inteligência Artificial
O objetivo desse trabalho é implementar dez perceptrons um para cada numeral árabe. Esses perceptrons irão identificar os casos do banco de dados [MNIST](yann.lecun.com/exdb/mnist/)

## Implementação
O programa foi feito em C++20 sob as especificações do banco de dados [MNIST](yann.lecun.com/exdb/mnist/): Imagens de 784 pixels (28px por 28px) em escalas de cinza representadas com 8 bits (0-255).

## Funcionalidades
* Exemplos de usabilidade;
* Opções de execução;
* etc etc

## Compilar o código
* Como compilar o código a partir de `cmake`;

## Integrantes
* [João Fukuda](https://gitlab.com/joao.fukuda/)
* [Júlia Passos](https://gitlab.com/juliacbpassos/)
* [Vinicius Hessel](https://gitlab.com//Hezsel/)

